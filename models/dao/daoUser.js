const { User } = require(`../userModel`);
const { BadRequestError } = require(`../../models/errorModel`);

const { userExists } = require(`../../routes/helpers/checkHelper`);

const findUserById = async (_id) => {
  if (!(await userExists({ _id }))) {
    throw new BadRequestError(`No user found!`);
  }

  return await User.findById(_id);
};

const findUserByName = async (username) => {
  if (!(await userExists({ username }))) {
    throw new BadRequestError(`No user found!`);
  }

  return await User.findOne({ username });
};

const deleteUserByUsername = async (username) => {
  await User.deleteOne({ username });
};

const checkCanditate = async (req, res, next) => {
  const { username } = req.body;
  if (await userExists({ username })) {
    throw new BadRequestError(`User ${username} already exists!`);
  }
  next();
};

module.exports = {
  findUserById,
  findUserByName,
  deleteUserByUsername,
  checkCanditate
};
