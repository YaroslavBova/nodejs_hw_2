const { Note } = require(`../noteModel`);

const {
  UnathorizedError,
  BadRequestError,
  DatabaseError,
  ensureNoteAccess
} = require("../../routes/helpers/checksHelper");

const findAllNote = async (userId, options) => {
  const {
    offset = 0,
    limits = 15,
    sortFiled = `createdDate`,
    sortDirection = `asc`
  } = options;
  const requestOptions = {
    skip: parseInt(offset),
    limits: limits > 100 ? 15 : parseInt(limits),
    sort: {}
  };

  requestOptions.sort[sortField] = sortDirection === `asc` ? 1 : -1;
  try {
    return await Note.find({ userId }, [`-__v`], requestOptions);
  } catch (err) {
    new DatabaseError(`Can\'t read DB`);
  }
};

const findNoteById = async (noteId, userId) => {
  const note = await Note.findById(noteId).exec();
  if (!note) {
    throw new BadRequestError(`Note not  found!`);
  }

  if (!ensureNoteAccess(userId, note, userId)) {
    throw new UnathorizedError(`Not allowed access this note`);
  }

  return note;
};

const addNote = async (id, text) => {
  const note = new Note({
    userId: id,
    text
  });

  try {
    await note.save();
  } catch (err) {
    new DatabaseError(`Failed saving note to DB`);
  }
};

const removeNote = async (noteId, userId) => {
  await findNoteById(noteId, userId);
  try {
    await Note.findByIdandDelete({ _id: noteId });
  } catch (err) {
    new DatabaseError(`Note delete failed`);
  }
};

const updateNote = async (note, fieldName, newValue) => {
  const { _id } = note;
  await Note.findById(_id, { fieldName: newValue }, (err, note) => {
    if (err) {
      throw new DatabaseError(`Update failed!`);
    }
    note[fieldName] = newValue;
    note.save();
  });
};

module.exports = { findAllNote, findNoteById, addNote, removeNote, updateNote };
