class ApplicationError extends Error {
  get name() {
    return this.constructor.name;
  }
}

class BadRequestError extends ApplicationError {
  constructor(message = "Bad Request") {
    super(message);
    this.statusCode = 400;
  }
}

class UnathorizedError extends ApplicationError {
  constructor(message = "Unathorized user") {
    super(message);
    this.statusCode = 401;
  }
}

class DatabaseError extends BadRequestError {
  constructor(message = "Database Request Error") {
    super(message);
  }
}

module.exports = {
  BadRequestError,
  UnathorizedError,
  DatabaseError
};
