const errorHadler = (err, req, res, next) => {
  if (err.statusCode) {
    return res
      .status(err.statusCode)
      .json({ message: err.message, stack: err.stack });
  }
  res.status(500).json({ message: "Internal server error" });
};

module.exports = { errorHadler };
