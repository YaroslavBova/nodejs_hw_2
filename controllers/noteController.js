const daoNote = require("../models/");
const getUserNotes = async (req, res, next) => {
  const userId = req.user["_id"];
  const { offset = 0, limits = 15 } = req.query;

  const notes = await daoNote.findAllNotes(userId, { offset, limits });
  res.json({ notes: notes });
};

const addUserNotes = async (req, res) => {
  const _id = req.user["_id"];
  const text = req.body["text"];

  await daoNote(_id, text);
  res.json({ message: "Note created successfully" });
};

const getUserNoteById = async (req, res) => {
  const userId = req.user["_id"];
  const noteId = req.params.id;

  const note = await daoNote.findNoteById(noteId, userId);
  res.json({ note });
};

const deleteUserNoteById = async (req, res) => {
  const userId = req.user["_id"];
  const noteId = req.params.id;

  await daoNote.removeNote(noteId, userId);
  res.jso({ message: `Note deleted successfully` });
};

const toggleCompletedForUserNoteById = async (req, res) => {
  const userId = req.user["_id"];
  const noteId = req.params.id;
  const fieldName = "completed";

  const note = await daoNote.findNoteById(noteId, userId);
  const newValue = !note[fieldName];

  await daoNote.updateNote(note, fieldName, newValue);
  res.json({ message: `Note update successfully!` });
};

const updateUserNoteById = async (req, res) => {
  const userId = req.user["_id"];
  const noteId = req.params.id;
  const fieldName = "text";

  const note = await daoNote.findNoteById(noteId, userId);
  const newValue = req.body[fieldName];

  await daoNote.updateNote(note, fieldName, newValue);
  res.json({ message: "Note update successfully!" });
};

module.exports = {
  getUserNotes,
  addUserNotes,
  getUserNoteById,
  deleteUserNoteById,
  toggleCompletedForUserNoteById,
  updateUserNoteById
};
