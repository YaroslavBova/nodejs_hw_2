const jwt = require(`jsonwebtoken`);
const config = require(`config`);
const JWT_SECRET = config.get(`JWT_SECRET`);

const { UnathorizedError } = require(`../../models/errorModel`);

const authMiddleware = async (req, res, next) => {
  const header = req.headers["authorization"];
  if (!header) {
    throw new UnathorizedError(`No Authorization header found!`);
  }

  const token = header.split(" ")[1];
  if (!token) {
    throw new UnathorizedError(`No JWT token found!`);
  }

  try {
    req.user = jwt.verify(token, JWT_SECRET);
  } catch (err) {
    next(new UnathorizedError(err.message));
  }

  next();
};

module.exports = { authMiddleware };
