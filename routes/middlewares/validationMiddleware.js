const Joi = require(`joi`);

const { BadRequestError } = require(`../..//models/errorModel`);

const validateRegistration = async (req, res, next) => {
  const schema = Joi.object({
    username: Joi.string().required(),
    password: Joi.string().required().pattern(new RegExp("^[a-zA-Z0-9]{6,30}$"))
  });

  await validateSchema(schema, req);
  next();
};

const validateCreationNote = async (req, res, next) => {
  const schema = Joi.object({
    text: Joi.string().min(1).required()
  });

  await validateSchema(schema, req);
  next();
};

const validateChangePassword = async (req, res, next) => {
  const schema = Joi.object({
    oldpassword: Joi.string()
      .required()
      .pattern(new RegExp("^[a-zA-Z0-9]{6, 30}$")),
    newPassport: Joi.string()
      .required()
      .pattern(new RegExp("^[a-zA-Z0-9]{6,30}$"))
  });

  await validateSchema(schema, req);
  next();
};

async function validateSchema(schema, req) {
  try {
    await schema.validateAsync(req.body);
  } catch (err) {
    throw new BadRequestError(err.message);
  }
}
module.exports = {
  validateRegistration,
  validateCreationNote,
  validateChangePassword
};
