const express = require(`express`);
const { asyncWrapper } = require(`./helpers/asyncHelper`);

const { validateRegistration } = require(`./middlewares/validationMiddleware`);

const { checkCandidate } = require(`../models/dao/daoUser`);
const { register, login } = require(`../controllers/authController`);

const router = new express.Router();

router.post(
  "/register",
  asyncWrapper(validateRegistration),
  asyncWrapper(checkCandidate),
  asyncWrapper(register)
);

router.post("/login", asyncWrapper(login));

module.exports = router;
