const { User } = require("../../models/userModel");

const userExists = async (fieldValue) => await User.exists(fieldValue);

const ensureNoteAccess = (reqUserId, noteUserId) => reqUserId === noteUserId;

module.exports = { userExists, ensureNoteAccess };
