const express = require(`express`);

const router = new express.Router();

const { asyncWrapper } = require(`./helpers/asyncHelper`);

const { authMiddleware } = require(`./middlewares/authMiddleware`);

const {
  validateChangePassword
} = require(`./middlewares/validationMiddleware`);

const {
  getProfileInfo,
  deleteProfile,
  changeProfilePassword
} = require(`../controllers/userController`);

router.use("/", asyncWrapper(authMiddleware));

router.get("/", asyncWrapper(getProfileInfo));
router.delete("/", asyncWrapper(deleteProfile));
router.patch(
  "/",
  asyncWrapper(validateChangePassword),
  asyncWrapper(changeProfilePassword)
);

module.exports = router;
