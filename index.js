const express = require("express");
const mongoose = require("mongoose");
const morgan = require("morgan");
const config = require(`config`);
const PORT = process.env.PORT || config.get("PORT") || 8080;
const MONGO_URL = config.get("MONGO_URI");

const {} = require("./controllers/");
const {} = require("./controllers/");
const {} = require("./middlewares/authMiddleware");

const app = express();
app.use(express.json());
app.use(morgan("tiny"));

app.use("/api/auth", authRouter);
app.use("/api/users/me", userRouter);
app.use("/api/notes", noteRouter);

app.use(errorHandler);

const start = async () => {
  try {
    await mongoose.connect(MONGO_URL, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useFindAndModify: false,
      createIndexes: true
    });
    app.listen(PORT, () => {
      console.log(`Server has been started on port ${PORT}`);
    });
  } catch (error) {
    console.error(`Error on server startup: ${error.message}`);
  }
};

start();
